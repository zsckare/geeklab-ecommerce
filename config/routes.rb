require 'api_constraints'

Rails.application.routes.draw do
  

  get 'mis-compras/', to: "purchase#index"

  get 'checkout/', to: "checkout#index"
  get 'cart/', to: "cart#index"
  post 'payment/card'
  get 'payment/card'
  post 'payment/paypal'
  post 'payment/oxxo'
  resources :orders
  resources :cart_items
  namespace :admin do
    root 'welcome#index'
    resources :categories
    resources :products
    resources :brands
  end

  resources :categories
  resources :products
  resources :brands
  root 'welcome#index'

  # Namespace for the API

  # Api with subdomain, api.mysite.org as '/...'
    # namespace :api,
    #   defaults: { format: :json }, constraints: { subdomain: 'api' }, path: '/'  do
    # end

  # Api uri formated mysite.org/api/...
  namespace :api, defaults: { format: :json } do
    scope module: :v1,
              constraints: ApiConstraints.new(version: 1, default: true) do
      # We are going to list our resources here
    end
  end

  # Change path to '/' if you want direct access to users views
   # Device acces for the users
  devise_for :users, path: '/', controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations',
    confirmations: 'users/confirmations',
    passwords: 'users/passwords',
    unlocks: 'users/unlocks',
    omniauth: 'users/omniauth'
  }

  #spanish path for devise
  devise_scope :user do
    get 'iniciar-sesion', to: 'users/sessions#new'
    get 'cerrar-sesion', to: 'users/sessions#destroy', as: :sign_out

    get 'registro', to: 'users/registrations#new'
    post 'sign_up', to: 'users/registrations#create'

    get 'recuperar-contraseña', to: 'users/passwords#new'
    get 'recuperar-contrasena', to: 'users/passwords#new'
    get 'nueva-contraseña', to: 'users/passwords#edit'
    get 'nueva-contrasena', to: 'users/passwords#new'

    get 'solicitar-confirmacion', to: 'users/confirmations#new'
    get 'confirmar-cuenta', to: 'users/confirmations#show'
  end

end
