
class PaymentController < ApplicationController

  def card
  	# Rescatamos el token de la tarjeta
    card_token = params[:conekta_token]
    begin
    folio = "#{params[:order_folio]}".to_i
   

    # puts "#{customer}--->#{card_token}"
    geeklab_order = Order.find(params[:order_id])


    # Recuperamos al cliente desde conekta
    puts "costumer id---->#{current_user.conekta_customer_id}"
    customer = Conekta::Customer.find(current_user.conekta_customer_id)

    @subtotal = 0
    geeklab_order.cart_items.each do|p|
      m = "#{p.product.price}".to_i*p.quantity
      @subtotal = @subtotal + m
    end

    # Vinculamos su tarjeta
    # SOLO SI EL USUARIO LO DESEA PARA CARGOS RECURRENTES
    source = customer.create_payment_source({
      :type => 'card',
      :token_id => card_token
    })


    @order = Conekta::Order.create({
      :line_items => [{
          :name => "Compra en GeekLabStore - #{folio}",
          :unit_price => (@subtotal*100).to_i,
          :quantity => 1
      }],
      :currency => "MXN",
      :customer_info => {
        :customer_id => current_user.conekta_customer_id
      },
      :charges => [{
          :payment_method => {
              :type => "card",
              :payment_source_id => source.id
          }
      }]
    })

    rescue Conekta::ErrorList => error_list
    	puts error_list.details[0].message
    	puts error_list.details[0].message_to_purchaser
    	
      redirect_to checkout_path, notice: error_list.details[0].message
    end

    unless @order.nil?
 
    puts "#{@order.payment_status}"

    if @order.payment_status == "paid"
      # p = OrderMailer.confirm_order(geeklab_order,current_user).deliver_now
      # redirect_to root_path
      geeklab_order.payment_method = "card"
      geeklab_order.status = 2
      geeklab_order.save
      render js: "okPay();"
    end

    end



  end

  def paypal

    order = Order.find(params[:order_id])
    order.state = params[:state]
    order.paypal_order_id = params[:paypal_id]
    order.payment_method = "paypal"
    order.status = 2
    order.amount = params[:amount]
    if order.save
      render json: order
    end

  end

  def oxxo
  end
end
