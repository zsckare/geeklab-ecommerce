class PurchaseController < ApplicationController
  def index
  	@orders = current_user.orders.where(status: 2)
  end
end
