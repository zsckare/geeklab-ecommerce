class Admin::BaseController < ApplicationController
	layout 'admin'  
	before_action :check_permision

	def check_permision
		if current_user.level !=1
			redirect_to root_path
		end
	end
end