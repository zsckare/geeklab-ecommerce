class CartItemsController < ApplicationController
  before_action :set_cart_item, only: [:show, :edit, :update, :destroy]

  # GET /cart_items
  # GET /cart_items.json
  def index
    @cart_items = current_user.cart_items
  end

  # GET /cart_items/1
  # GET /cart_items/1.json
  def show
  end

  # GET /cart_items/new
  def new
    @cart_item = CartItem.new
  end

  # GET /cart_items/1/edit
  def edit
  end

  # POST /cart_items
  # POST /cart_items.json
  def create
    if !user_signed_in?
      redirect_to new_user_session_path
    end
    @order = 0
    orders = current_user.orders.where(status: 1)
    if orders.count == 0
      folio = Digest::SHA1.hexdigest([Time.now, rand].join)[0..10]
      @order = Order.create(status: 1, description: "",amount: 0.0,user_id: current_user.id, folio: folio)
    else
      @order = orders.last
    end
    
 
    
    puts "------>#{current_user.id}"
    puts "#{cart_item_params}"
    @cart_item = CartItem.new
    @cart_item.product_id = params[:cart_item][:product_id]
    @cart_item.price = params[:cart_item][:price]
    @cart_item.quantity = params[:cart_item][:quantity]
    @cart_item.user_id = current_user.id
    @cart_item.order_id = @order.id
    if @cart_item.save
      render json: @cart_item
    end
    # respond_to do |format|
    #   if @cart_item.save
    #     format.html { redirect_to @cart_item, notice: 'Cart item was successfully created.' }
    #     format.json { render :show, status: :created, location: @cart_item }
    #   else
    #     format.html { 
    #       puts "#{@cart_item.errors}"
    #       render :new }
    #     format.json { 
    #       puts "#{@cart_item.errors}"
    #       render json: @cart_item.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PATCH/PUT /cart_items/1
  # PATCH/PUT /cart_items/1.json
  def update
    respond_to do |format|
      if @cart_item.update(cart_item_params)
        format.html { redirect_to @cart_item, notice: 'Cart item was successfully updated.' }
        format.json { 
          p = @cart_item
        item = {
                id:p.id,
                name: p.product.name,
                product_id: p.product.id,
                price: p.product.price,
                description: p.product.description,
                image: p.product.image.url,
                quantity: p.quantity
              }
          render json: item }
      else
        format.html { render :edit }
        format.json { 


          render json: @cart_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cart_items/1
  # DELETE /cart_items/1.json
  def destroy
    @cart_item.destroy
    respond_to do |format|
      format.html { redirect_to cart_items_url, notice: 'Cart item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cart_item
      @cart_item = CartItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cart_item_params
      params.require(:cart_item).permit(:product_id, :quantity, :price, :user_id)
    end
end
