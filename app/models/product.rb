class Product < ApplicationRecord
  belongs_to :brand
  has_many :has_categories
  after_create :save_categories
  mount_uploader :image, FotoUploader

  def categories=(vaue)
  	@categories=vaue
  end

  def save_categories
  	@categories.each do |cat|
  		HasCategory.create(category_id: cat, product_id: self.id)
  	end
  end
end
