class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_many :orders
  has_many :cart_items
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :recoverable, :validatable, :lockable
   after_create :welcome_email

   def welcome_email
   		UserMailer.welcome_email(self).deliver
   end
end
