class UserMailer < ApplicationMailer
	  default from: "tokyo.blue.tsubasa@gmail.com"

	def welcome_email(user)
		puts "ENVIANDO ---->#{user.email}"
		@user = user
		mail(to: @user.email, subject: 'Bienvenido') 	
	end
end
