class OrderMailer < ApplicationMailer

	def confirm_order(order, user)

		puts "Enviando correo de confirmacion de orden"
		puts "creando orden de compra"
		@order = order
		@user = user
		@subtotal = 0
		@order.cart_items.each do|p|
			puts "#{p.product.name}-------#{p.product.price}----------#{p.quantity}"
			m = "#{p.product.price}".to_i * p.quantity
			puts "subtotal ---->#{m}"
			@subtotal = @subtotal+m
		end


		mail(to: @user.email, subject: 'Resumen de compra') 	
	end
end
