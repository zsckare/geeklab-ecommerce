shopping_cart = [];


function add_to_shopping_cart(product_id){
    var source   = $("#cart-item-template").html();
    var template = Handlebars.compile(source);
    var shop = [];
    lockr = Lockr.get('shopping_cart');
    if(lockr !=null){
      shop = lockr;
    }
    uri = "/products/"+product_id+".json";
    $.ajax({
      type: 'GET',
      url: uri,
      async: false,      
      dataType: 'json',
      success: function (data) {
        // console.log(data.id);
        item = {
          name: data.name,
          description: data.description,
          id: data.id,
          price: data.price,
          image: data.image,
          quantity: 1
        };
        shop.push(item);
        Lockr.set("shopping_cart",shop);
        var a = Lockr.get('shopping_cart');
        // console.log(a);
        h = template(data);
        $("#cart").append(h);
        swal("Se ha agregado al carrito de compra");
        // add_to_cart(id,price)
      }
    });

}

function remove_from_shopping_cart(index){

}

function plus_item(index){

  item = shopping_cart[index];
  console.log(item);

  item.quantity = item.quantity+1;
  shopping_cart[index] = item;
  // console.log("new item in index");
  // console.log(shopping_cart[index]);
  Lockr.set("shopping_cart",shopping_cart);
  buildCartTable();
}
function minus_item(index){
 item = shopping_cart[index];
  console.log(item);
  var count = item.quantity;
  if (count >0) {
    item.quantity = item.quantity-1;
    shopping_cart[index] = item;
    // console.log("new item in index");
    // console.log(shopping_cart[index]);
    Lockr.set("shopping_cart",shopping_cart);

  }
  buildCartTable();

}

function buildCartTable(){
    $("#cart-item-container").html("");
    var source   = $("#cart-item-template-tr").html();
    var template = Handlebars.compile(source);
    lockr  = Lockr.get('shopping_cart');
    var cart_items = [];
    if (lockr !=null) {
      cart_items = lockr;
      shopping_cart = lockr;
    }
    var subtotal = 0;
    for(var i = 0; i < cart_items.length; i++){
      var data = cart_items[i];
      // console.log(data);
      img = data.image.url;
      price = data.price * data.quantity
      item = {
          index: i,
          name: data.name,
          description: data.description,
          id: data.id,
          price: price,
          image: img,
          quantity: data.quantity
      };
      subtotal = subtotal+price;
      // console.log(item);

      var hg = template(item);
      $("#cart-item-container").append(hg);
    }
    $("#subtotal").html("$"+subtotal);
}

function afterCheckout(){

  var cart_items = Lockr.get('shopping_cart');
  // console.log(cart_items);

  data = cart_items;
  console.log(cart_items);
  if(cart_items.length >0){
    product = cart_items[0];
    item = {
                  product_id: product.id,
                  price: product.price,
                  quantity: product.quantity
              };
              $.ajax({
                data: {
                  cart_item:item
                },
                url: "/cart_items",
                type: "POST",
                success:  function (response) {
                  hacerFor();
            
                }
              });

    // $.ajax({
    //   data: {
    //     cart_item:cart_items[0]
    //   },
    //   url: "/cart_items",
    //   type: "POST",
    //   success:  function (response) {
        
    //       for (var i = 1; i < cart_items.length; i++) {
    //          console.log(cart_items[i]);
    //         product = cart_items[i];
    //         item = {
    //             product_id: product.id,
    //             price: product.price,
    //             quantity: product.quantity
    //         };
    //         console.log(item);
    //         saveCartItemRails(item);
    //       }
    //       document.location.href = "/checkout";
        
    //   }
    // });
  
  }
  


  
}
function hacerFor(){
  var cart_items = Lockr.get('shopping_cart');
  console.log("Hacer for");
  console.log(cart_items.length);
for (var i = 1; i <cart_items.length; i++) {
   console.log("Entra for");
             console.log(cart_items[i]);
            product = cart_items[i];
            item = {
                product_id: product.id,
                price: product.price,
                quantity: product.quantity
            };
            console.log(item);
            saveCartItemRails(item);
          }
          document.location.href = "/checkout";
}

function saveCartItemRails(item) {

  $.ajax({
    data: {
      cart_item:item
    },
    url: "/cart_items",
    type: "POST",
    success:  function (response) {
      

    }
  });
}

function payWithPayPay(paypalObj,order_id){
  url = "/payment/paypal";
  paypal_id = paypalObj.id;
  console.log("------------");
   a  = $("#subtotal_2").val();
  state = paypalObj.state;
  $.ajax({
    data: {
      paypal_id: paypal_id,
      state: state,
      order_id: order_id,
      amount: a
    },
    url: url,
    type: "POST",
    success:  function (response) {
      Lockr.flush();
      console.log(response);
      document.location.href = "/"
    }
  });
}






