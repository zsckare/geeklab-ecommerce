// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//

// = require jquery
//= require jquery_ujs
//= require bootstrap
//= require turbolinks
//= require_tree .








var puerto =window.location.port;
var urlhost="http://" + window.location.hostname;


cart_items = []

function addItem(id){
  var source   = $("#cart-item-template").html();
  var template = Handlebars.compile(source);

    uri = "/products/"+id+".json"
        $.ajax({
      type: 'GET',
      url: uri,
      async: false,
      beforeSend: function (xhr) {
        if (xhr && xhr.overrideMimeType) {
          xhr.overrideMimeType('application/json;charset=utf-8');
        }
      },
      dataType: 'json',
      success: function (data) {
        console.log(data.id);
        item = {
          name: data.name,
          description: data.description,
          id: data.id,
          price: data.price,
          image: data.image
        };
        cart_items.push(item);
        console.log("------>");
        console.log(item);
        Lockr.set("cart_items",cart_items);
        var a = Lockr.get('cart_items');
        console.log(a);
        var price = parseFloat(data.price);
        h = template(data);
        $("#cart").append(h);
        add_to_cart(id,price)
      }
    });

}

function add_to_cart(id,price){

    uri= "/cart_items";
    $.ajax({
        type:'POST',
        url: uri,
        async: true,
        beforeSend: function (xhr) {
        if (xhr && xhr.overrideMimeType) {
          xhr.overrideMimeType('application/json;charset=utf-8');
        }
      },
      data:{
        cart_item:{
            product_id: id,
            price: price
        }
      },
      dataType: 'json',
      success: function (data) {
        console.log(data);
        swal("Se ha agrego al carrito de compras");
        
      }

    });
}

function getCartItems(){
  console.log("getItems");
  $("#cart").html("");
  var arr = Lockr.get('cart_items');
  //console.log(arr);
  var source   = $("#cart-item-template").html();
  var template = Handlebars.compile(source);
  if (arr != null) {
    for(var i = 0; i < arr.length; i++){
      var data = arr[i];
      console.log("i->"+i);
      console.log(data.image.url);
      img = data.image.url;
      item = {
          name: data.name,
          description: data.description,
          id: data.id,
          price: data.price,
          image: img
      };
      // console.log(item);
      var hg = template(item);
      $("#cart").append(hg);
    }
  }
}


items_cart = [];
function buildCartContainer(){
  
  items_cart = $('#data_model_name').data('your-data');
  console.log(items_cart);
  var source   = $("#cart-item-template-tr").html();
  var template = Handlebars.compile(source);

    for(var i = 0; i < items_cart.length; i++){
      var data = items_cart[i];
      console.log("i->"+i);
      img = data.image;
      price = data.price * data.quantity
      item = {
          name: data.name,
          description: data.description,
          id: data.id,
          price: price,
          image: img,
          quantity: data.quantity
      };
      // console.log(item);
      var hg = template(item);
      $("#cart-item-container").append(hg);
    }

}

function plus(id,quantity){
  console.log("-----------------------------plus");
  uri = "/cart_items/"+id;
t =quantity+1;
  $.ajax({
    type:'PATCH',
    url: uri,
    async: true,
    beforeSend: function (xhr) {
        if (xhr && xhr.overrideMimeType) {
          xhr.overrideMimeType('application/json;charset=utf-8');
        }
      },
      data:{
        cart_item:{
            
            quantity: t
        }
      },
      dataType: 'json',
      success: function (data) {
        console.log(data);
        var source =$("#cart-item-template-tr-inner").html();
        var template = Handlebars.compile(source);
        price = data.price * data.quantity;
        item = {
          name: data.name,
          description: data.description,
          id: data.id,
          price: price,
          image: data.image,
          quantity: data.quantity
      };
        var h = template(item);
        $("#content_"+id).html(h);
        
      }

    });
}

function minus(id, quantity){
console.log("-----------------------------plus");
  uri = "/cart_items/"+id;
  var t = 1;
  if (quantity ==1) {
    t = 1;
  }
  if (quantity > 1) {
    t = quantity-1;
  }

  $.ajax({
    type:'PATCH',
    url: uri,
    async: true,
    beforeSend: function (xhr) {
        if (xhr && xhr.overrideMimeType) {
          xhr.overrideMimeType('application/json;charset=utf-8');
        }
      },
      data:{
        cart_item:{
            
            quantity: t
        }
      },
      dataType: 'json',
      success: function (data) {
        console.log(data);
        var source =$("#cart-item-template-tr-inner").html();
        var template = Handlebars.compile(source);
        price = data.price * data.quantity;
        item = {
          name: data.name,
          description: data.description,
          id: data.id,
          price: price,
          image: data.image,
          quantity: data.quantity
      };
        var h = template(item);
        $("#content_"+id).html(h);
        
      }

    });
}

function okPay() {
  // swal("Pago Aceptado","","success");
  swal({
    title: 'Pago Aceptado',
    text: ''
  }).then(
    function () {
      document.location.href = '/';
    },
    // handling the promise rejection
    function (dismiss) {
      document.location.href = '/'
    }
  );
}


function validShippingInputs() {
  
  var response = true;
  var msg = "";
  if ($("#shipping_name").val().length == 0) {
    response = false;
    msg += "-Nombre. \n";
  }

  if ($("#shipping_dir").val().length == 0) {
    response = false;
    msg += "-Direccion. \n";
  }
  if ($("#shipping_estado").val().length == 0) {
    response = false;
    msg += "-Estado. \n";
  }

  if ($("#shipping_city").val().length == 0) {
    response = false;
    msg += "-Cuidad/Municipio. \n";
  }

  if ($("#shipping_col").val().length == 0) {
    response = false;
    msg += "-Colonia/Fraccionamiento. \n";
  }
  if ($("#shipping_postal").val().length == 0) {
    response = false;
    msg += "-Codigo Postal. \n";
  }



  if (response==false) {
    $('#myModal').modal('hide');
    swal("Estos campos son obligatorios", msg,"error");
  }


  return response;
}

//---conekta-----
Conekta.setPublishableKey('key_DPpukPxjcL5CMNw64pSAquA');

function createToken() {
  card_name = $("#card_name").val();
  card_number = $("#card_number").val();
  card_cvc = $("#card_cvc").val();
  card_month = $("#card_month").val();
  card_year = $("#card_year").val();

  var data = {
  "card": {
    "number": card_number,
    "name": card_name,
    "exp_year": card_year,
    "exp_month": card_month,
    "cvc": card_cvc
  }
  };
  var successHandler = function(token) {
    /* token keys: id, livemode, used, object */
    console.log(token);
    $("#conektaTokenId").val(token.id);

    var res = validShippingInputs();
    if (res==true) {
      payWithCard();
    }
  };

  var errorHandler = function(err) {
    /* err keys: object, type, message, message_to_purchaser, param, code */
    console.log(err);
  };

  Conekta.Token.create(data, successHandler, errorHandler);
    

}
function payWithCard() {
  $('#myModal').modal('hide');
  $.snackbar({content: "Espere por favor, procesando pago."});
  conekta_token = $("#conektaTokenId").val();
  shipping_name = $("#shipping_name").val();
  shipping_dir = $("#shipping_dir").val();
  shipping_estado = $("#shipping_estado").val();
  shipping_city = $("#shipping_city").val();
  shipping_col = $("#shipping_col").val();
  shipping_postal = $("#shipping_postal").val();
  uid = $("#uid").val();
  order_id = $("#order_id").val();
  order_folio = $("#order_folio").val();

  var url = "/payment/card";

  $.ajax({
    type:'POST',
    url: url,
    async: true,
    
    data:{
      conekta_token: conekta_token,
      order_folio: order_folio,
      order_id: order_id
    },
    success:function(data) {
      Lockr.flush();
    },
    error: function (jqXHR, exception) {
        var msg = '';
        if (jqXHR.status === 0) {
            msg = 'Not connect.\n Verify Network.';
        } else if (jqXHR.status == 404) {
            msg = 'Requested page not found. [404]';
        } else if (jqXHR.status == 500) {
            msg = 'Internal Server Error [500].';
        } else if (exception === 'parsererror') {
            msg = 'Requested JSON parse failed.';
        } else if (exception === 'timeout') {
            msg = 'Time out error.';
        } else if (exception === 'abort') {
            msg = 'Ajax request aborted.';
        } else {
            msg = 'Uncaught Error.\n' + jqXHR.responseText;
        }
        swal(msg);
    }

  });
}

$(document).on('ready page:load ',function(){
	
    $("#product_brand_id").addClass("form-control");
    getCartItems();

  //plugin bootstrap minus and plus
//http://jsfiddle.net/laelitenetwork/puJ6G/
$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {
    
    minValue =  1;
    maxValue =  100;
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        
        $(this).val($(this).data('oldValue'));
    }
    
    
});
$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });


    $("#product_brand_id").addClass('form-control');
	  $('[data-toggle="tooltip"]').tooltip();
 $(function () {
    $('.navbar-toggle').click(function () {
        $('.navbar-nav').toggleClass('slide-in');
        $('.side-body').toggleClass('body-slide-in');
        $('#search').removeClass('in').addClass('collapse').slideUp(200);

        /// uncomment code for absolute positioning tweek see top comment in css
        //$('.absolute-wrapper').toggleClass('slide-in');
        
    });
   
   // Remove menu for searching
   $('#search-trigger').click(function () {
        $('.navbar-nav').removeClass('slide-in');
        $('.side-body').removeClass('body-slide-in');

        /// uncomment code for absolute positioning tweek see top comment in css
        //$('.absolute-wrapper').removeClass('slide-in');

    });
});



});
