json.extract! order, :id, :status, :description, :amount, :created_at, :updated_at,:payment_method,:state
json.url order_url(order, format: :json)
