class AddConektaCostumerIdToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :conekta_customer_id, :string
  end
end
