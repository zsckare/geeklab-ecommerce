class AddPayPalCheckOutIdToOrder < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :paypal_order_id, :string
    add_column :orders, :conekta_order_id, :string
    add_column :orders, :state, :string
  end
end
