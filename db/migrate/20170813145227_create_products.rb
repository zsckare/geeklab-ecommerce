class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :name
      t.string :model
      t.string :price
      t.string :description
      t.references :brand, foreign_key: true

      t.timestamps
    end
  end
end
