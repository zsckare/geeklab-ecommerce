class AddFolioToOrder < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :folio, :string
  end
end
