class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.integer :status
      t.text :description
      t.float :amount

      t.timestamps
    end
  end
end
