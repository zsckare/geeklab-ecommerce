# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


users = User.create([{email: "admin@admin.com",password: "geeklab", level: 1}])

brands = Brand.create([{name: "Marca 01"},{name: "Marca 2"}])

categories = Category.create([
	{id:1,name: "Categoria 1"},
	{id:2,name: "Categoria 2"},
	{id:3,name: "Categoria 3"},
	{id:4,name: "Categoria 4"},
	{id:5,name: "Categoria 5"}])



descc = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.a"
products = Product.create([
	{name: "Producto 1",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[1,2,3,4]},
	{name: "Producto 2",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[1,2,3,4]},
	{name: "Producto 3",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[1,2,3,4]},
	{name: "Producto 4",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[1,2,3,4]},
	{name: "Producto 5",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[1,2,3,4]},
	{name: "Producto 6",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[1,2,4]},
	{name: "Producto 7",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[1,5]},
	{name: "Producto 8",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[1,2,3,4]},
	{name: "Producto 9",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[1,5,3]},
	{name: "Producto 10",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[2,4]},
	{name: "Producto 11",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[2,3,4]},
	{name: "Producto 12",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[2,3,4]},
	{name: "Producto 13",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[2,3,5]},
	{name: "Producto 14",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[1,2,4,5]},
	{name: "Producto 15",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[1,2]},
	{name: "Producto 16",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[5,3]},
	{name: "Producto 17",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[4]},
	{name: "Producto 18",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[5,4]},
	{name: "Producto 19",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[4]},
	{name: "Producto 20",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[1,4]},
	{name: "Producto 21",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[1,2,3,4]},
	{name: "Producto 22",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[1,4]},
	{name: "Producto 23",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[1,3]},
	{name: "Producto 24",model: "modelo 1", price: "1000",brand_id: 1, description: "#{descc}",image: "https://lorempixel.com/300/400/",categories:[2,3,4]}
	])


