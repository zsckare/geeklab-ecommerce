require 'rails_helper'

RSpec.describe Admin::PaymentController, type: :controller do

  describe "GET #card" do
    it "returns http success" do
      get :card
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #paypal" do
    it "returns http success" do
      get :paypal
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #oxxo" do
    it "returns http success" do
      get :oxxo
      expect(response).to have_http_status(:success)
    end
  end

end
