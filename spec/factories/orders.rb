FactoryGirl.define do
  factory :order do
    status 1
    description "MyText"
    amount 1.5
  end
end
