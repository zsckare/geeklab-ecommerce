FactoryGirl.define do
  factory :product do
    name "MyString"
    model "MyString"
    price "MyString"
    description "MyString"
    brand nil
  end
end
