FactoryGirl.define do
  factory :cart_item do
    product nil
    quantity 1
    price "MyString"
    user nil
  end
end
