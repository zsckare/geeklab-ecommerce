require 'rails_helper'

RSpec.describe "products/show", type: :view do
  before(:each) do
    @product = assign(:product, Product.create!(
      :name => "Name",
      :model => "Model",
      :price => "Price",
      :description => "Description",
      :brand => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Model/)
    expect(rendered).to match(/Price/)
    expect(rendered).to match(/Description/)
    expect(rendered).to match(//)
  end
end
