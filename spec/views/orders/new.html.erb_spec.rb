require 'rails_helper'

RSpec.describe "orders/new", type: :view do
  before(:each) do
    assign(:order, Order.new(
      :status => 1,
      :description => "MyText",
      :amount => 1.5
    ))
  end

  it "renders new order form" do
    render

    assert_select "form[action=?][method=?]", orders_path, "post" do

      assert_select "input#order_status[name=?]", "order[status]"

      assert_select "textarea#order_description[name=?]", "order[description]"

      assert_select "input#order_amount[name=?]", "order[amount]"
    end
  end
end
