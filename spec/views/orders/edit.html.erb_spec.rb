require 'rails_helper'

RSpec.describe "orders/edit", type: :view do
  before(:each) do
    @order = assign(:order, Order.create!(
      :status => 1,
      :description => "MyText",
      :amount => 1.5
    ))
  end

  it "renders the edit order form" do
    render

    assert_select "form[action=?][method=?]", order_path(@order), "post" do

      assert_select "input#order_status[name=?]", "order[status]"

      assert_select "textarea#order_description[name=?]", "order[description]"

      assert_select "input#order_amount[name=?]", "order[amount]"
    end
  end
end
