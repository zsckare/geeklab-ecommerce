class UserMailerPreview < ActionMailer::Preview
  # Preview this email at http://localhost:3000/rails/mailers/contact_mailer/feedback
  def welcome_email
    user = User.all.sample
    message = "Test message."
    UserMailer.welcome_email(user)
  end
end