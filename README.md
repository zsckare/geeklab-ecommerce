# rails-app-base
An Rails 5.0 applications with all that you need!!

# Add configuration values to config/application.yml, as shown below.

MYSQL_USER: "Your_Mysql_user"
MYSQL_PASSWORD: "Your_Mysql_password"

CLOUDINARY_CLOUD: "your_cloudinary_cloud_name"
CLOUDINARY_API_KEY: "your_cloudinary_api_key"
CLOUDINARY_API_SECRET: "your_cloudinary_api_secret"

API_HOST: "your_host_name"   or   API_HOST: "api.your_host_name"
SECRET_KEY_BASE: "acb0293182d7f89e79a"
